##================================================================================================
                                        Beat Machine
##================================================================================================
# Rerefência
Esse é o repositório do projeto Beat Machine.
Utilizamos como referência o [Git]: https://github.com/hatsumatsu/108

# Testando e Executando
Para executar os testes nesse projeto você vai precisar de:
1. `Node.js` (npm por padrão)
2. Instalar `npm` e `grunt` na pasta do projeto
3. Com servidor local(`XAMPP` ou similar) inicie o `Apache`
4. Execute a aplicação via `localhost`